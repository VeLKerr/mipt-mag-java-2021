package org.atp.lesson9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsPractice {
    private static final String ROOD_DIR = "./griboedov";

    private static void example1(){
        long[] fibResult = Stream.iterate(new long[]{0L, 1L}, t -> new long[]{t[1], t[0] + t[1]})
                .mapToLong(t -> t[0]).peek(System.out::println).limit(50).toArray();
//                .forEach(System.out::println);
    }

    private static void generateNums(int max){
        ThreadLocalRandom.current().ints(0, max).limit(500).distinct().sorted().forEach(System.out::println);
    }

    private static void wordCount() throws IOException {
        Stream<Path> innerFiles = Files.walk(Paths.get(ROOD_DIR)).filter(Files::isRegularFile);
//        innerFiles.forEach(System.out::println);

        // Map<String, Integer> wordcount =
        Map<Object, Long> wordcount = Files.lines((Path) innerFiles.toArray()[0]).flatMap(str -> Arrays.stream(str.trim().split("\\s+")))
                .map(word -> word.replaceAll("[^a-zA-Z0-9а-яА-Я]", "").toLowerCase())
                .filter(word -> word.length() > 0)
                .map(word -> new AbstractMap.SimpleEntry<>(word, 1))
                .collect(Collectors.groupingBy(AbstractMap.SimpleEntry::getKey, Collectors.counting()));
//                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue, Integer::sum));
        wordcount.forEach((k, v) -> System.out.println(k + "\t" + v));
    }

    public static void main(String[] args) throws IOException {
        wordCount();
    }
}
