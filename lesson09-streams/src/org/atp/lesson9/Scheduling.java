package org.atp.lesson9;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.*;

public class Scheduling {
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss");
                    System.out.println("Scheduled at " + df.format(LocalDateTime.now()));
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println("Finished at " + df.format(LocalDateTime.now()));
                }
                catch (InterruptedException e){
                    System.err.println("Interrupted :(");
                }
            }
        };

        ScheduledFuture<?> future = executorService.scheduleAtFixedRate(task, 0, 3, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(2);
//        ScheduledFuture<?> future2 = executorService.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(20);
        executorService.shutdown();
    }
}
