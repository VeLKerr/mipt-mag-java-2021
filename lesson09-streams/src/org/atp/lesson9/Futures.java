package org.atp.lesson9;

import java.util.concurrent.*;

public class Futures {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> task = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int sleepingTime = 3;
                TimeUnit.SECONDS.sleep(sleepingTime);
                return sleepingTime;
            }
        };

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> resultFuture = executorService.submit(task);
        System.out.println("Is done? \n -" + resultFuture.isDone());
        int result = 0;
        try {
            result = resultFuture.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            System.err.println("Too early!");
        }
        result = resultFuture.get();
        System.out.println(result);
        int result2 = resultFuture.get();
        executorService.shutdown();
    }
}
