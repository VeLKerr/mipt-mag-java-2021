package com.example.bookstore.service;

import com.example.bookstore.dto.Book;
import java.util.List;

public interface BookService {
     String getBookInfo(Long id);

     Book saveBook(Book book);

     List<Book> getAllBooks();

     void removeBook(Long id);

}
