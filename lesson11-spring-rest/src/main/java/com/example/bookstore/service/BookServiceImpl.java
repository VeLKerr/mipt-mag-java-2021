package com.example.bookstore.service;

import com.example.bookstore.dto.Book;
import com.example.bookstore.exceptions.BookIdMismatchException;
import com.example.bookstore.repository.BookRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override public String getBookInfo(Long id) {
        Optional<Book> bookOpt = bookRepository.findById(id);

        String res = "";

        if (bookOpt.isPresent()) {
            res = "Book with aithor: " + bookOpt.get().getAuthor() + " " +
                "and description: " + bookOpt.get().getDescription();
        }
        else {
            throw new BookIdMismatchException("can't pull info by id");
        }
        return res;
    }

    @Override public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    @Override public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override public void removeBook(Long id) {
        bookRepository.deleteById(id);
    }
}
