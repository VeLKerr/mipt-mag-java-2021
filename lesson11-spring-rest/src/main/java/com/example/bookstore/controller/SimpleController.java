package com.example.bookstore.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {
    String appName;

    @RequestMapping("/")
    public String homePage(Model model){
//        model.addAllAttributes("appName", appName);
        return "Hello from home page";
    }
}
