package com.dineshonjava.sbsecurity.service;

import com.dineshonjava.sbsecurity.bean.CurrentUser;

public interface CurrentUserService {
	
	 boolean canAccessUser(CurrentUser currentUser, Long userId);
}
