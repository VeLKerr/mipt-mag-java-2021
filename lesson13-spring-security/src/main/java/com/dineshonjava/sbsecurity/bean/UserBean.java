package com.dineshonjava.sbsecurity.bean;

import javax.validation.constraints.NotNull;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import com.dineshonjava.sbsecurity.role.Role;

@Data
public class UserBean {
	@NotEmpty
    private String username = "";
	
	@NotEmpty
    private String email = "";

    @NotEmpty
    private String password = "";

    @NotEmpty
    private String passwordRepeated = "";

    @NotNull
    private Role role = Role.USER;
}
