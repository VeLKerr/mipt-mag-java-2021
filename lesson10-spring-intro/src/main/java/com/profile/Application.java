package com.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

public class Application {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("dev");

        ctx.register(ProfilesConfiguraion.class);
        ctx.refresh();

        DevDataConfig dataConfig = ctx.getBean(DevDataConfig.class);
        dataConfig.setup();

        ctx.close();
    }
}
