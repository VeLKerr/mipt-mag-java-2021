package com.profile;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevDataConfig implements DataSourceConfig {
    @Value("${dataconfig.default}")
    String dataConfig;

    @Override public void setup() {
        System.out.println("dev config: " + dataConfig);
    }
}



