package com.example.simplewebapplication;

import java.util.Arrays;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SimpleWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimpleWebApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        return args -> {
            System.out.println("Beans in our application: ");
            String[] beans = context.getBeanDefinitionNames();
            Arrays.sort(beans);
            for (String bean :
                beans) {
                System.out.println("bean: " + bean);
            }
        };
    }

}
