package com.example.simplewebapplication;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {
    @RequestMapping("/hello")
    public String helloagain() {
        return "Hello world again!";
    }

    @RequestMapping("/")
    public String hello() {
        return "Hello world!";
    }
}
