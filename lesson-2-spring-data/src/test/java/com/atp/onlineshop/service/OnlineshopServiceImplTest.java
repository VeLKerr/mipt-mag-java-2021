package com.kloeckner.onlineshop.service;

import com.kloeckner.onlineshop.OnlineshopAbstractTest;
import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import com.kloeckner.onlineshop.repository.CategoryRepository;
import com.kloeckner.onlineshop.repository.ProductRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles({"test"})
public class OnlineshopServiceImplTest extends OnlineshopAbstractTest {
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private ProductRepository productRepository;
    @InjectMocks
    private OnlineshopServiceImpl shopService;

    @BeforeEach
    void setUp() {
        createItems();
    }

    @Test
    void testFindByCategory() {
        List<Product> expectalProdcuts = Arrays.asList(product1, product2);
        assertEquals(shopService.findByCategory(INCLUDED_CATEGORY_1), expectalProdcuts);
        assertEquals(shopService.findByCategory(INCLUDED_CATEGORY_1).size(), 2);
    }

    @Test
    void testFindByBrand() {
        List<Product> expectalProdcuts = Collections.singletonList(product1);
        assertEquals(shopService.findByBrand(INCLUDED_BRAND_1), expectalProdcuts);
        assertEquals(shopService.findByBrand(INCLUDED_BRAND_1).size(), 1);
    }

    @Test
    void testShowAllCategories() {
        List<Category> expectalCategories = Arrays.asList(category1, category2);

        assertEquals(shopService.findAllCategories(), expectalCategories);
    }

    private void createItems() {
        when(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_1)).
            thenReturn(Collections.singletonList(product1));
        when(categoryRepository.findAllByName(INCLUDED_CATEGORY_1)).thenReturn(Collections.singletonList(category1));
        when(categoryRepository.findAll()).thenReturn(Arrays.asList(category1, category2));
    }
}
