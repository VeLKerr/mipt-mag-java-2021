package com.kloeckner.onlineshop;

import com.kloeckner.onlineshop.dto.Attributes;
import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import java.util.Arrays;

/*
Abstract class  for all type of tests. Consists of the basic information about products and categories
which are used as a test data.
As a test instances 4 products and 2 categories are created.
1st and 2nd products are from "mobiles" category, 3d and 4th from "laptops". There is also one fake category,
"tablets", that is not registered in repository. Also, there is one more category, "laptops", which is not included
to the list.
 */
public abstract class OnlineshopAbstractTest {
    // two products in this category: galaxy and iphone
    protected final String INCLUDED_CATEGORY_1 = "mobiles";
    // two products in this category: mac pro and ultrabook
    protected final String INCLUDED_CATEGORY_2 = "laptops";
    // this category is empty
    protected final String NOT_INCLUDED_CATEGORY = "tablets";

    // two products with this brand: iphone and mac pro
    protected final String INCLUDED_BRAND_1 = "apple";
    // one product with this brand: galaxy
    protected final String INCLUDED_BRAND_2 = "samsung";
    // one product with this brand: ultrabook
    protected final String INCLUDED_BRAND_3 = "lenovo";

    protected final String DESCRIPTION_1 = "iphone";
    protected final String DESCRIPTION_2 = "galaxy";
    protected final String DESCRIPTION_3 = "mac pro";
    protected final String DESCRIPTION_4 = "ultrabook";

    protected final String BRAND_FAKE = "xiaomi mi9 pro";

    protected Category category1;
    protected Category category2;

    protected Product product1;
    protected Product product2;
    protected Product product3;
    protected Product product4;

    protected OnlineshopAbstractTest() {
        Attributes attributes1 = new Attributes(111, 5.5, INCLUDED_BRAND_1, "ios");
        product1 = new Product(11, DESCRIPTION_1, 1500, attributes1);
        Attributes attributes2 = new Attributes(121, 13, INCLUDED_BRAND_2, "android");
        product2 = new Product(12, DESCRIPTION_2, 1000, attributes2);
        category1 = new Category(1, INCLUDED_CATEGORY_1, Arrays.asList(product1, product2));

        Attributes attributes3 = new Attributes(211, 16, INCLUDED_BRAND_1, "osx");
        product3 = new Product(21, DESCRIPTION_3, 2500, attributes3);
        Attributes attributes4 = new Attributes(221, 13, INCLUDED_BRAND_3, "microsoft");
        product4 = new Product(22, DESCRIPTION_4, 1500, attributes4);
        category2 = new Category(2, INCLUDED_CATEGORY_2, Arrays.asList(product3, product4));
    }
}
