package com.kloeckner.onlineshop.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.kloeckner.onlineshop.OnlineshopAbstractTest;
import com.kloeckner.onlineshop.repository.CategoryRepository;
import com.kloeckner.onlineshop.repository.ProductRepository;
import java.util.Arrays;
import java.util.Collections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/*
Class for OnlineShopController class.  Tests checks that api requests return correct products and categories.
 */
@ActiveProfiles({"test"})
@RunWith(JUnitPlatform.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OnlineshopControllerTest extends OnlineshopAbstractTest {
    @Value("http://localhost:${local.server.port}/kloeckner/onlineshop")
    private String baseUrl;
    private MockMvc mvc;

    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private ProductRepository productRepository;

    @Autowired
    public WebApplicationContext context;

    @BeforeEach
    void setUp() {
        assertNotNull(context);

        this.mvc = MockMvcBuilders.webAppContextSetup(context)
            .build();
        context.getServletContext().setAttribute(
            WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, context);

        createItems();
    }

    @Test
    void testRequestAllCategories() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/categories")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();

        Assertions.assertTrue(content.contains(INCLUDED_CATEGORY_1));
        Assertions.assertTrue(content.contains(INCLUDED_CATEGORY_2));

        Assertions.assertFalse(content.contains(NOT_INCLUDED_CATEGORY));
    }

    @Test
    void testRequestByBrand() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/products?brand=" + INCLUDED_BRAND_1)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();
        Assertions.assertTrue(content.contains(DESCRIPTION_1));
        Assertions.assertTrue(content.contains(DESCRIPTION_3));

        Assertions.assertFalse(content.contains(DESCRIPTION_2));
        Assertions.assertFalse(content.contains(DESCRIPTION_4));
    }

    @Test
    void testRequestByBrandFailed() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/products?brand=" + BRAND_FAKE)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();
        Assertions.assertFalse(content.contains(BRAND_FAKE + "</td>"));
    }

    @Test
    void testRequestByCategory() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/products?category=" + INCLUDED_CATEGORY_1)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();

        Assertions.assertTrue(content.contains(DESCRIPTION_1));
        Assertions.assertTrue(content.contains(DESCRIPTION_2));

        Assertions.assertFalse(content.contains(DESCRIPTION_3));
        Assertions.assertFalse(content.contains(DESCRIPTION_4));

    }

    @Test
    void testRequestByCategoryFailed() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/products?category=" + NOT_INCLUDED_CATEGORY)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();

        // there is no rows in the table
        Assertions.assertFalse(content.contains("</td>"));

    }

    @Test
    void testAllProducts() throws Exception {
        MvcResult result = mvc.perform(get(baseUrl + "/products")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
            .andReturn();

        String content = result.getResponse().getContentAsString();

        Assertions.assertTrue(content.contains(DESCRIPTION_1));
        Assertions.assertTrue(content.contains(DESCRIPTION_2));
        Assertions.assertTrue(content.contains(DESCRIPTION_3));
        Assertions.assertTrue(content.contains(DESCRIPTION_4));

        Assertions.assertFalse(content.contains(BRAND_FAKE + "</td>"));

    }

    private void createItems() {
        when(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_1)).thenReturn(Arrays.asList(product1, product3));
        when(categoryRepository.findAllByName(INCLUDED_CATEGORY_1)).thenReturn(Collections.singletonList(category1));

        when(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_2)).thenReturn(Arrays.asList(product2, product4));
        when(categoryRepository.findAll()).thenReturn(Arrays.asList(category1, category2));

        when(productRepository.findAll()).thenReturn(Arrays.asList(product1, product2, product3, product4));
    }
}
