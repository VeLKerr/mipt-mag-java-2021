package com.kloeckner.onlineshop;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;
import org.junit.platform.runner.JUnitPlatform;

/*
  To run all tests in one click.
*/
@RunWith(JUnitPlatform.class)
@SelectPackages("com.kloeckner.onlineshop")
public class OnlineshopTestSuite {
}
