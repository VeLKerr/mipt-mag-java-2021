package com.kloeckner.onlineshop.repository;

import com.kloeckner.onlineshop.OnlineshopAbstractTest;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ActiveProfiles({"test"})
public class CategoryRepositoryTest extends OnlineshopAbstractTest {
    @BeforeEach
    void setUp() {
        createItems();
    }

    @Autowired
    protected CategoryRepository categoryRepository;

    @Test
    public void testfindAllByName() {
        assertEquals(categoryRepository.findAllByName(INCLUDED_CATEGORY_1).get(0).getName(), INCLUDED_CATEGORY_1);
        assertEquals(categoryRepository.findAllByName(INCLUDED_CATEGORY_2).get(0).getName(), INCLUDED_CATEGORY_2);
        assertTrue(categoryRepository.findAllByName(NOT_INCLUDED_CATEGORY).isEmpty());
    }

    @Test
    public void testfindAll() {
        assertEquals(categoryRepository.findAll().get(0).getName(), INCLUDED_CATEGORY_1);
        assertEquals(categoryRepository.findAll().get(1).getName(), INCLUDED_CATEGORY_2);
        assertEquals(categoryRepository.findAll().size(), 2);
    }

    private void createItems() {
        categoryRepository.saveAll(Arrays.asList(category1, category2));
    }
}
