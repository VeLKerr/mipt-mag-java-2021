package com.kloeckner.onlineshop.repository;

import com.kloeckner.onlineshop.OnlineshopAbstractTest;
import java.util.Arrays;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ActiveProfiles({"test"})
public class ProductRepositoryTest extends OnlineshopAbstractTest {
    @BeforeEach
    void setUp() {
        createItems();
    }

    @Autowired
    protected ProductRepository productRepository;

    @Test
    public void testfindByAttributeBrand() {
        assertEquals(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_1), Arrays.asList(product1, product3));
        assertEquals(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_2), Collections.singletonList(product2));
        assertEquals(productRepository.findAllByAttributesBrand(INCLUDED_BRAND_3), Collections.singletonList(product4));
        assertTrue(productRepository.findAllByAttributesBrand("xiaomi").isEmpty());
    }

    private void createItems() {
        productRepository.saveAll(Arrays.asList(product1, product2, product3, product4));
    }
}
