package com.kloeckner.onlineshop.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@Entity
@AllArgsConstructor
public class Product {
    @Id
    private int id;

    private String description;

    private double price;

    @OneToOne(cascade = CascadeType.ALL)
    private Attributes attributes;

    public Product() {

    }
}
