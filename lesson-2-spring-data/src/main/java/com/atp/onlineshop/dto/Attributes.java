package com.kloeckner.onlineshop.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@Entity
@AllArgsConstructor
public class Attributes {
    @Id
    private int id;

    private double size;

    private String brand;

    private String os;

    public Attributes() {

    }
}
