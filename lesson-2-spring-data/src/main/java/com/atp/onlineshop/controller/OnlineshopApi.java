package com.kloeckner.onlineshop.controller;

import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import java.util.List;
import java.util.Optional;

public interface OnlineshopApi {
    // @return either list of products sorted by optional parameter or list of all products.
    List<Product> findProducts(Optional<String> brand, Optional<String> category);

    // @return all saved categories.
    List<Category> findAllCategories();
}
