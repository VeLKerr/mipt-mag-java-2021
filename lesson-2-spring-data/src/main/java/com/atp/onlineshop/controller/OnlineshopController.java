package com.kloeckner.onlineshop.controller;

import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import com.kloeckner.onlineshop.service.OnlineshopService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/kloeckner/onlineshop")
public class OnlineshopController implements OnlineshopApi {
    private final OnlineshopService onlineshopService;

    @GetMapping("/categories")
    @ModelAttribute("categories")
    @Override
    public List<Category> findAllCategories() {
        return onlineshopService.findAllCategories();
    }

    @GetMapping("/products")
    @ModelAttribute("products")
    @Override
    public List<Product> findProducts(@RequestParam(value = "brand") Optional<String> brand,
        @RequestParam(value = "category") Optional<String> category) {
        if (brand.isPresent())
            return onlineshopService.findByBrand(brand.get());
        else if (category.isPresent())
            return onlineshopService.findByCategory(category.get());
        else
            return onlineshopService.findAllProducts();
    }

}
