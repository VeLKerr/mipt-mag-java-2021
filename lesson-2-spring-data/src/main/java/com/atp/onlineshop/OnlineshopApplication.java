package com.kloeckner.onlineshop;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kloeckner.onlineshop.dto.CategoryWrapper;
import com.kloeckner.onlineshop.service.OnlineshopService;
import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import org.springframework.context.annotation.Profile;

@Slf4j
@SpringBootApplication
public class OnlineshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineshopApplication.class, args);
    }

    @Bean
    @Profile({"!test"})
    CommandLineRunner runner(OnlineshopService onlineshopService) {
        return args -> {
            // Read json and write to h2.
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            TypeReference<CategoryWrapper> typeReference = new TypeReference<CategoryWrapper>() {
            };
            InputStream inputStream = TypeReference.class.getResourceAsStream("/json/sample_data.json");
            try {
                CategoryWrapper categories = mapper.readValue(inputStream, typeReference);
                onlineshopService.save(new ArrayList<>(categories.getCategories()));
                log.info("Categories are saved.");
            }
            catch (IOException e) {
                log.error("Unable to save categories: " + e.getMessage());
            }
        };
    }
}
