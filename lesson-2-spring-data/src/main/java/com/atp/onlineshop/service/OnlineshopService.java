package com.kloeckner.onlineshop.service;

import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import java.util.List;

public interface OnlineshopService {
    List<Product> findAllProducts();

    List<Category> findAllCategories();

    List<Product> findByCategory(String category);

    List<Product> findByBrand(String brand);

    void save(List<Category> categories);
}
