package com.kloeckner.onlineshop.service;

import com.kloeckner.onlineshop.dto.Category;
import com.kloeckner.onlineshop.dto.Product;
import com.kloeckner.onlineshop.repository.CategoryRepository;
import com.kloeckner.onlineshop.repository.ProductRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OnlineshopServiceImpl implements OnlineshopService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    @Override public List<Product> findAllProducts() {
        log.info("Show all products.");
        return productRepository.findAll();
    }

    @Override
    public List<Category> findAllCategories() {
        log.info("Show all categories.");

        return categoryRepository.findAll();
    }

    @Override
    public List<Product> findByCategory(String categoryName) {
        log.info("Get products from category [{}]", categoryName);
        List<Category> categories = categoryRepository.findAllByName(categoryName);
        if (categories.isEmpty()) {
            log.warn("No such category: [{}]", categoryName);
            return new ArrayList<>();
        }
        else
            return categories.get(0).getProducts();
    }

    @Override
    public List<Product> findByBrand(String brandName) {
        log.info("Get products from category [{}]", brandName);
        List<Product> products = productRepository.findAllByAttributesBrand(brandName);
        if (products.isEmpty()) {
            log.warn("No such brand: [{}]", brandName);
        }
        return products;
    }

    @Override
    public void save(List<Category> categories) {
        categoryRepository.saveAll(categories);
    }
}
