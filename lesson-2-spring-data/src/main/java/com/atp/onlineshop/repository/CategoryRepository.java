package com.kloeckner.onlineshop.repository;

import com.kloeckner.onlineshop.dto.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, String> {
    List<Category> findAllByName(String name);
}
