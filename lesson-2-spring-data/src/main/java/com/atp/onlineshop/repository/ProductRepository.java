package com.kloeckner.onlineshop.repository;

import com.kloeckner.onlineshop.dto.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByAttributesBrand(String brand);
}
