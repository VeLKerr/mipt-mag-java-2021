package org.atp.lesson8;

import org.atp.lesson8.annotation.BuilderProperty;

public class Person {
    private String name;
    private int age;
    private String address;
    private int salary;
    private double averagePoints;

    public double getAveragePoints() {
        return averagePoints;
    }

    @BuilderProperty
    public void setAveragePoints(double averagePoints) {
        this.averagePoints = averagePoints;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @BuilderProperty
    public void setAddress(String address) {
        this.address = address;
    }

    @BuilderProperty
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @BuilderProperty
    public String getName() {
        return name;
    }

    @BuilderProperty
    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
