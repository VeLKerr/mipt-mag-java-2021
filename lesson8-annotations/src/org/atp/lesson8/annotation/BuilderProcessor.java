package org.atp.lesson8.annotation;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ExecutableType;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedAnnotationTypes("org.atp.lesson8.annotation.BuilderProperty")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BuilderProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        for(TypeElement annotation: annotations){
            List<Element> annotatedElements = retrieveSetters(annotation, roundEnvironment);
            String className = ((TypeElement) annotatedElements.get(0).getEnclosingElement()).getQualifiedName().toString();
            Map<String, String> setterParameters = retrieveParameters(annotatedElements);
            try {
                writeBuilderFile(className, setterParameters);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private List<Element> retrieveSetters(TypeElement annotation, RoundEnvironment roundEnvironment){
        Set<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(annotation);
        Map<Boolean, List<Element>> methods = annotatedElements.stream().collect(
                Collectors.partitioningBy(element ->
                        element.getSimpleName().toString().startsWith("set") &&
                                ((ExecutableType) element.asType()).getParameterTypes().size() == 1)
        );
        for(Element el: methods.get(false)){
            processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Error applying an annotation", el);
        }
        return methods.get(true);
    }

    private Map<String, String> retrieveParameters(List<Element> setters){
        Map<String, String> setterParameters = new HashMap<>();
        for(Element setter: setters){
            setterParameters.put(
                    setter.getSimpleName().toString(),
                    ((ExecutableType) setter.asType()).getParameterTypes().get(0).toString()
            );
        }
        return setterParameters;
    }

    private void writeBuilderFile(String className, Map<String, String> setters) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }

        String simpleClassName = className.substring(lastDot + 1);
        String builderClassName = className + "Builder";
        String builderSimpleClassName = builderClassName.substring(lastDot + 1);

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }

            out.print("public class ");
            out.print(builderSimpleClassName);
            out.println(" {");
            out.println();

            out.print("    private ");
            out.print(simpleClassName);
            out.print(" object = new ");
            out.print(simpleClassName);
            out.println("();");
            out.println();

            out.print("    public ");
            out.print(simpleClassName);
            out.println(" build() {");
            out.println("        return object;");
            out.println("    }");
            out.println();

            setters.entrySet().forEach(setter -> {
                String methodName = setter.getKey();
                String argumentType = setter.getValue();

                out.print("    public ");
                out.print(builderSimpleClassName);
                out.print(" ");
                out.print(methodName);

                out.print("(");

                out.print(argumentType);
                out.println(" value) {");
                out.print("        object.");
                out.print(methodName);
                out.println("(value);");
                out.println("        return this;");
                out.println("    }");
                out.println();
            });

            out.println("}");
        }
    }
}
