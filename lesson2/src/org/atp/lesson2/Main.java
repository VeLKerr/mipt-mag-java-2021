package org.atp.lesson2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Student student = new Student("Oleg", "Ivchenko", "o@velkerr.ru");
        Student anotherStudent = new Student("Test", "Test", "test@gmail.com");
//        System.out.println(student.getName());
        System.out.println(student);
        Group gr094a = new Group(new Student[]{
                student, anotherStudent
        });
//        gr094a.addStudent(student);
//        System.out.println(gr094a);
        System.out.println(Arrays.toString(gr094a.getStudents()));
        gr094a.deleteStudent(student);
        System.out.println(Arrays.toString(gr094a.getStudents()));

        List<Object> il = new ArrayList<>();
        il.add(new Object());
        Object a = il.get(0);

        HashSet<Integer> hs = new HashSet<>();
        hs.add(5);
        hs.add(2);

        System.out.println(hs);
    }
}
