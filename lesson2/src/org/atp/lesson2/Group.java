package org.atp.lesson2;

import java.util.Arrays;

public class Group {
    private static int CNTER = 0;
    private int id;
    private Student[] students;

    public Group() {
        this.id = CNTER;
        CNTER++;
    }

    public Group(Student[] students) {
        this();
        this.students = students.clone();
    }

    public void addStudent(Student student){
        if(this.students == null){
            this.students = new Student[1];
        }
        else {
            Student[] tmp = new Student[this.students.length + 1];
            System.arraycopy(this.students, 0, tmp, 0, students.length);
            this.students = tmp;
        }
        this.students[this.students.length - 1] = student;
    }

    public boolean deleteStudent(Student student){
        int removeIndex = -1;
        for(int i = 0; i < students.length; i++){
            if(this.students[i].equals(student)){
                removeIndex = i;
                break;
            }
        }
        if(removeIndex < 0){
            return false;
        }
        Student[] tmp = new Student[students.length - 1];
        System.arraycopy(students, 0, tmp, 0, removeIndex);
        System.arraycopy(students, removeIndex + 1, tmp, removeIndex, students.length - (removeIndex + 1));
        students = tmp;
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
