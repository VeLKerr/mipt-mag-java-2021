package org.atp.lesson3.enums;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Seasons[] coldSeasons = new Seasons[]{
//                Seasons.WINTER, Seasons.SPRING, Seasons.AUTUMN
//        };
//        Seasons s = Seasons.AUTUMN;
//        for (Seasons cs: coldSeasons){
//            if(s == cs){
//                System.out.println("It's cold!");
//            }
//        }
        Seasons s = Seasons.AUTUMN;
        System.out.println(s.ordinal());
        System.out.println(s.getYear());
        System.out.println(Seasons.valueOf("AUTUMN").isCold());
        System.out.println(s.isCold());

        List<String> fields = new ArrayList<>();
        fields.add("Octets");
    }
}
