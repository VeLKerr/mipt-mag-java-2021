package org.atp.lesson3.enums;

public enum Seasons {
    WINTER(2019){
        public boolean isCold(){return true;}
    },
    SPRING(2020){
        public boolean isCold(){return true;}
    },
    SUMMER(2021){
        public boolean isCold(){return false;}
    },
    AUTUMN(2016){
        public boolean isCold(){return true;}
    };

    private int year;

    Seasons(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public abstract boolean isCold();

//    public boolean isCold(){
//        Seasons[] coldSeasons = new Seasons[]{
//                Seasons.WINTER, Seasons.SPRING, Seasons.AUTUMN
//        };
//        for(Seasons s: coldSeasons){
//            if(s == this){
//                return true;
//            }
//        }
//        return false;
//    }
}
