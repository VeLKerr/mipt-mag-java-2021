package org.atp.lesson3;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

class A{
    String name = "Class A";
    String getName() {
        return name;
    }
}
class B extends A{
    String name = "Class B"; // Никогда так не делайте (не переопределяйте поля) !!!
    String getName() {
        return name;
    }
}
public class AB{
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        A ab = new B();
        System.out.println("a : " + a.name + " " + a.getName());
        System.out.println("b : " + b.name + " " + b.getName());
        System.out.println("ab: " + ab.name + " " + ab.getName());
        // AB$<hash>
        LinkedList<Integer> linkedList = new LinkedList<Integer>(){

        };
    }
}
