package org.atp.lesson3;

class StaticTest3 {
    static int myStatic;
    int myNonStatic;

    static StaticTest3 staticMethod() {
        myStatic = 1; // OK
//        myNonStatic = 2; // ERROR !
        new StaticTest3().myNonStatic = 2;
//        new StaticTest3().nonStatic();
    // return this; //ERROR !
        return new StaticTest3(); // OK
    }

    public void nonStatic(){
        System.out.println(this);
    }
}
