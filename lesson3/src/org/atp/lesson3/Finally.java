package org.atp.lesson3;

import java.io.IOException;

public class Finally {
    public static void mainOld(String[] args) {
        try {
            System.out.println("Start");
            throw new RuntimeException();
//            System.exit(0);
        }
        finally {
            System.out.println("Finally!");
            return;
        }
    }

    public static void main(String[] args) {
        System.out.println(method());
//        while (true);
//        return;
    }

    private static int method(){
        while (true);

        //Java puzzlers
    }
}
