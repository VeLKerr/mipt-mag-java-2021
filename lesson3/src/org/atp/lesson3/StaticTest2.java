package org.atp.lesson3;

class St2 {
    static int myStatic = returnIntSayHello();

    static int returnIntSayHello() {
        System.out.println("Hello");
        return 1;
    }

    public St2() {
        System.out.println("Constructor");
    }
}

public class StaticTest2 {
    private static St2 st2 = new St2();

    public static void main(String[] args) {
//        St2 st2 = new St2();
        System.out.println("Entry point");
    }
}
