package org.atp.lesson3.static_exercises;

import java.util.Arrays;

class A {
    public static int unusedMethod(){
        System.out.println("I'm unused :(");
        return 1;
    }

    static final int a1=printStringReturnInt("a1");

    static {
        printStringReturnInt("static block");
        int [] array = new int[5];
        Arrays.fill(array, 1);
        System.out.println(array[array.length]);
    }

    A() {
        printStringReturnInt("Constructor");
//        int [] array = new int[5];
//        Arrays.fill(array, 1);
//        System.out.println(array[array.length]);
    }

    static int a2 = printStringReturnInt("a2");

    public static int printStringReturnInt(String s) {
        System.out.println(s);
        return 0;
    }
}

public class Main {
    static {
        System.out.println("Before main");
    }

    public static void main(String[] args) {
        System.out.println("Begin of main");
        try {
            A a = new A();
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.err.println("Incorrect constructor");
        }
        catch (ExceptionInInitializerError e){
            System.err.println("Errrorrr!");
        }

        System.out.println("End of main");
    }

    static {
        System.out.println("After main");
    }
}
