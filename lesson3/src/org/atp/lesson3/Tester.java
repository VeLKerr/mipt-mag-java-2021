package org.atp.lesson3;

import java.util.Arrays;
//import static java.lang.Math.sin;
import static java.lang.Math.*;

public class Tester {
    public String method() throws Exception{
        try {
            throw new Exception("1");
        }
        catch (Exception e){
            throw new Exception("2");
        }
        finally {
            return "3";
        }
    }

    public static void main(String[] args) {
        Tester t = new Tester();
        try {
            System.out.println(t.method());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        new Tester().toString();
    }
}
