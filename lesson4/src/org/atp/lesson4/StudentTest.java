package org.atp.lesson4;

import org.junit.*;

public class StudentTest {
    private Student st;

    @BeforeClass
    public static void setupClass(){
        System.out.println("Hello!");
    }

    @AfterClass
    public static void teardownClass(){
        System.out.println("Bye!");
    }

    @Before
    public void setUp(){
        System.out.println("Current test is starting");
        st = new Student("Ivan", "Ivanov", "test@mail.ru");
    }

    @After
    public void tearDown(){
        System.out.println("Current test is finished");
    }

    @Test
    public void getEmail() {
        String expectedEmail = "oleg@velkerr.ru";
        Student st = new Student("Oleg", "Ivchenko", expectedEmail);
        Assert.assertEquals("Incrrect email", expectedEmail, st.getEmail());
    }

    @Test
    public void changeName(){
        String expectedName = "Anton";
        st.setName(expectedName);
    }

    @Test
    public void testName(){
        String expectedName = "Anton";
        Assert.assertEquals("Names aren't equal", expectedName, st.getName());
    }

    @Test
    public void testExceptionBad(){
        String test = null;
        try {
            boolean result = test.isEmpty();
        }
        catch (NullPointerException npe){
            Assert.assertTrue(true);
        }
    }

    @Test(expected = NullPointerException.class)
    public void testExceptionGood(){
        String test = null;
        boolean result = test.isEmpty();
    }

    @Test
    public void compareDifferentStudents(){
        Student st2 = new Student("Ivan", "Ivanov", "test2@mail.ru");
        Assert.assertEquals(st, st2);
    }

    @Test
    @Ignore
    public void setEmail() {
        Assert.fail("Fail");
    }

    public void someMethod(){
        System.out.println("??");
    }
}