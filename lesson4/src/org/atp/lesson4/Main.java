package org.atp.lesson4;

import java.util.*;

public class Main {
    private static void fill(Collection<Integer> coll){
        int len = 8;
        for(int i=1; i<len; i++){
            if(i==4){
                System.out.println("tt");
            }
            else{
                System.out.println("ff");
            }
            System.out.println((i==4)?"tt":"ff");
            coll.add(i);
        }

        Group gr = new Group();
    }

    public static void main(String[] args) {
        Collection<Integer> coll = new ArrayList<>();
        fill(coll);
        coll.remove(5);
        System.out.println(coll);

        List<Integer> lst = new ArrayList<>();
        fill(lst);
        lst.remove((int)5);
        System.out.println(lst);
    }

}
