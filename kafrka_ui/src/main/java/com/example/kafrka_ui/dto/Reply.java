package com.example.kafrka_ui.dto;

import java.util.HashSet;
import java.util.Set;

public class Reply {
    private Set<String> mailAddress;

    private Set<String> domain;

    public Reply(){
        mailAddress = new HashSet<>();
        domain = new HashSet<>();
    }

    public Set<String> getDomain() {
        return domain;
    }

    public Set<String> getMailAddress() {
        return mailAddress;
    }
}
