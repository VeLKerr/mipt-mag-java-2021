package com.example.kafrka_ui.dto;

import javax.persistence.GeneratedValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailAddress {
    @Id
    @GeneratedValue
    Long id;

    String mailAddress;
}
