package com.example.kafrka_ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaUiApplication.class, args);
    }

}
