package com.example.kafrka_ui.controller;

import com.example.kafrka_ui.dto.Reply;
import java.util.Collection;

public interface KafkaEmailStreamController {
    void getMailAddress(String mailAddress);

    void getMailAddress(Collection<String> mailAddress);

    Collection<Reply> uniqueAddressesAndDomain();
}
