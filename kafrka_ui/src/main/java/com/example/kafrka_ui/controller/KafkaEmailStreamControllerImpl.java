package com.example.kafrka_ui.controller;

import com.example.kafrka_ui.dto.Reply;
import com.example.kafrka_ui.service.KafkaEmailStreamService;
import java.util.Collection;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/unitedInternet/emailsCounter")
@RequiredArgsConstructor
public class KafkaEmailStreamControllerImpl implements KafkaEmailStreamController{
    private final KafkaEmailStreamService service;

    @Override
    @GetMapping("/")
    public void getMailAddress(String mailAddress) {

    }

    @Override
    @GetMapping("/")
    public void getMailAddress(Collection<String> mailAddress) {

    }

    @Override
    @PostMapping("/unique")
    public Collection<Reply> uniqueAddressesAndDomain() {

        return null;
    }
}
