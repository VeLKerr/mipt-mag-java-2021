package com.example.kafrka_ui.repository;

import com.example.kafrka_ui.dto.MailAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KafkaEmailStreamRepository extends JpaRepository<MailAddress, Long> {

}
