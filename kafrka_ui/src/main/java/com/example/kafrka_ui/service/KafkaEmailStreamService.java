package com.example.kafrka_ui.service;

import com.example.kafrka_ui.dto.MailAddress;
import com.example.kafrka_ui.dto.Reply;
import java.util.Collection;

public interface KafkaEmailStreamService {
    void saveAddress(MailAddress address);

    void saveAllAddresses(Collection<MailAddress> address);

    Reply findUnique();
}
