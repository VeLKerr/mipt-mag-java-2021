package com.example.kafrka_ui.service;

import com.example.kafrka_ui.dto.MailAddress;
import com.example.kafrka_ui.dto.Reply;
import com.example.kafrka_ui.repository.KafkaEmailStreamRepository;
import java.util.Collection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaEmailStreamServiceImpl implements KafkaEmailStreamService {
    private final KafkaEmailStreamRepository emailStreamRepository;

    @Override public void saveAddress(MailAddress address) {
        emailStreamRepository.save(address);
    }

    @Override public void saveAllAddresses(Collection<MailAddress> addresses) {
        emailStreamRepository.saveAll(addresses);
    }

    @Override public Reply findUnique() {
        Collection<MailAddress> allAddresses = emailStreamRepository.findAll();
        Reply reply = new Reply();
        for (MailAddress address :
            allAddresses) {
            reply.getMailAddress().add();
        }

        return reply;
    }
}
